package com.walmart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author Srikanth Dugyala
 * 
 * Ticket Service Application class which is the entry point to the application
 *
 */
@SpringBootApplication
@EnableScheduling
public class TicketserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TicketserviceApplication.class, args);

	}
}
